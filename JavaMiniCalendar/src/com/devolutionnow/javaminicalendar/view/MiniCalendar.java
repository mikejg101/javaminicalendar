/*
 * The MIT License
 *
 * Copyright 2015 Michael Goodwin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.devolutionnow.javaminicalendar.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Michael Goodwin
 */
public class MiniCalendar extends JFrame implements ActionListener, MouseListener {

    // Size Instance Variables
    private Dimension screenSize;
    private int spacer;

    // Header Instance Variables
    private final String[] header;
    private final String title = "Calendar";

    // Date and Time Instance Variables
    private int yearRange;
    private Date chosenDate;
    private GregorianCalendar baseCalendar;
    private int day;
    private int month;
    private int year;
    private int currentMonth;
    private int currentYear;
    private final String[] months;

    // Labels Instance Variables
    private JLabel monthLabel;
    private JLabel yearLabel;
    private JLabel emptyLabel;

    // Button and Button Image Instance Variables
    private JButton nextButton;
    private JButton previousButton;
    private JButton closeButton;

    // Combo Box
    private JComboBox yearComboBox;

    // Container
    private Container containerPane;

    // JPanels
    private JPanel navigationPanel;
    private JPanel calendarPanel;
    private JPanel choicePanel;

    // Calendar Table Instance Variables
    private DefaultTableModel calendarTableModel;
    private JTable calendarTable;

    // JScrollPane
    private JScrollPane calendarScrollPane;

    private void Initialize() {

        // Set Behavior
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        // Set size, spacer and year range.
        setDefaultCalendarSize();
        setSpacer(25);

        // Create Labels and set alignment.
        monthLabel = new JLabel("January");
        monthLabel.setHorizontalAlignment(JLabel.CENTER);
        yearLabel = new JLabel("Change year: ");
        emptyLabel = new JLabel();

        // Create Buttons
        nextButton = new JButton();
        previousButton = new JButton();
        closeButton = new JButton(new AbstractAction("Close") {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton buttonPressed = (JButton) e.getSource();
                if (buttonPressed == closeButton) {
                    System.exit(0);
                }
            }
        });

        // Add button action listeners
        nextButton.addActionListener(this);
        previousButton.addActionListener(this);

        // Attempt to add image to buttons
        createNavigationButton(nextButton, "Forward24.gif");
        createNavigationButton(previousButton, "Back24.gif");

        // Create Button and Month Panel, set layout and add components
        navigationPanel = new JPanel() {
            // Set navigation panel background colors
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                int w = getWidth();
                int h = getHeight();

                // Set gradient light color
                Color color2 = new Color(164, 195, 245);

                // Set gradient dark color
                Color color1 = new Color(102, 118, 201);

                // Create gradient
                GradientPaint gp = new GradientPaint(0, 0, color1, 0, h, color2);

                // Apply gradient
                g2d.setPaint(gp);
                g2d.fillRect(0, 0, w, h);
            }

            @Override
            public void setBorder(Border border) {
                super.setBorder(BorderFactory.createSoftBevelBorder(10)); //To change body of generated methods, choose Tools | Templates.
            }

        };
        navigationPanel.setLayout(new GridLayout(0, 3));
        navigationPanel.add(previousButton);
        navigationPanel.add(monthLabel);
        navigationPanel.add(nextButton);

        // Create Combo Box and add action listener
        yearComboBox = new JComboBox();
        yearComboBox.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (yearComboBox.getSelectedItem() != null) {
                    String b = yearComboBox.getSelectedItem().toString();
                    currentYear = Integer.parseInt(b);
                    refreshCalendar(currentMonth, currentYear);
                }
            }
        });

        // Create choice panel, set layou and add components
        choicePanel = new JPanel() {
            // Set choice panel panel background colors
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                int w = getWidth();
                int h = getHeight();

                // Set gradient light color
                Color color1 = new Color(164, 195, 245);

                // Set gradient dark color
                Color color2 = new Color(102, 118, 201);

                // Create gradient
                GradientPaint gp = new GradientPaint(0, 0, color1, 0, h, color2);

                // Apply gradient
                g2d.setPaint(gp);
                g2d.fillRect(0, 0, w, h);
            }

            @Override
            public void setBorder(Border border) {
                super.setBorder(BorderFactory.createSoftBevelBorder(10)); //To change body of generated methods, choose Tools | Templates.
            }
        };
        choicePanel.setLayout(new GridLayout(0, 3));
        choicePanel.add(closeButton);
        choicePanel.add(yearLabel);
        choicePanel.add(yearComboBox);

        // Create and setup date and time compnents
        setYearRange(10);
        baseCalendar = new GregorianCalendar();

        // Create and setup calendar table
        calendarTableModel = new DefaultTableModel() {

            // Prevent cell editing
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        calendarTable = new JTable(calendarTableModel);
        calendarTable.addMouseListener(this);

        // Disable Resizing
        calendarTable.getTableHeader().setResizingAllowed(false);

        // Disable Reordering
        calendarTable.getTableHeader().setReorderingAllowed(false);

        // Set to single cell selection
        calendarTable.setColumnSelectionAllowed(true);
        calendarTable.setRowSelectionAllowed(true);
        calendarTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        calendarTable.setCellSelectionEnabled(true);

        // Set and Show Grid
        calendarTable.setGridColor(Color.BLACK);
        calendarTable.setShowGrid(true);

        // Create ScrollPane for calendar table
        calendarScrollPane = new JScrollPane(calendarTable);

        // Create calendar panel and set layout
        calendarPanel = new JPanel(null) {

            // Set table background colors
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                int w = getWidth();
                int h = getHeight();

                // Set gradient light color
                Color color1 = Color.LIGHT_GRAY;

                // Set gradient dark color
                Color color2 = Color.DARK_GRAY;

                // Create gradient
                GradientPaint gp = new GradientPaint(0, 0, color1, 0, h, color2);

                // Apply gradient
                g2d.setPaint(gp);
                g2d.fillRect(0, 0, w, h);
            }
        };

        calendarPanel.setLayout(new BorderLayout());
        calendarPanel.add(navigationPanel, BorderLayout.NORTH);
        calendarPanel.add(calendarScrollPane, BorderLayout.CENTER);
        calendarPanel.add(choicePanel, BorderLayout.SOUTH);

        // Create container and set layout
        containerPane = getContentPane();
        containerPane.setLayout(new GridLayout());
        containerPane.add(calendarPanel);

        // Set time variables
        day = baseCalendar.get(GregorianCalendar.DAY_OF_MONTH);
        month = baseCalendar.get(GregorianCalendar.MONTH);
        year = baseCalendar.get(GregorianCalendar.YEAR);
        currentMonth = month;
        currentYear = year;

        // Set Header
        for (int i = 0; i < 7; i++) {
            calendarTableModel.addColumn(header[i]);
        }

        //Set row/column count
        calendarTableModel.setColumnCount(7);
        calendarTableModel.setRowCount(6);

        System.out.println(calendarScrollPane.getHeight());

        calendarTable.setRowHeight(getHeight() / 10);

        //Populate combo box
        for (int i = year - yearRange; i <= year + yearRange; i++) {
            yearComboBox.addItem(String.valueOf(i));
        }

        refreshCalendar(month, year);

    }

    private void createNavigationButton(JButton navButton, String fileName) {

        // Get rid of button borders
        navButton.setBorderPainted(false);
        navButton.setBorder(null);

        // Try to set image for button
        try {
            navButton.setIcon(new ImageIcon(ImageIO.read(getClass().getResource(fileName))));
        } catch (IOException | IllegalArgumentException ex) {

            // Reset borders and put text instead of image
            navButton.setBorderPainted(true);
            if (navButton == nextButton) {
                navButton.setText(">>");
            } else if (navButton == previousButton) {
                navButton.setText("<<");
            }

        }
    }

    private void refreshCalendar(int month, int year) {
        int numberOfDays, beginningOfMonth;

        //Allow/disallow buttons
        previousButton.setEnabled(true);
        nextButton.setEnabled(true);
        if (month == 0 && year <= year - yearRange) {
            previousButton.setEnabled(false);
        } //Too early
        if (month == 11 && year >= year + yearRange) {
            nextButton.setEnabled(false);
        } //Too late
        monthLabel.setText(months[month]); //Refresh the month label (at the top)
        //monthLabel.setBounds(160 - lblMonth.getPreferredSize().width / 2, 25, 180, 25); //Re-align label with calendar
        yearComboBox.setSelectedItem(String.valueOf(year)); //Select the correct year in the combo box

        //Clear table
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                calendarTableModel.setValueAt(null, i, j);
            }
        }

        //Get first day of month and number of days
        GregorianCalendar cal = new GregorianCalendar(year, month, 1);
        numberOfDays = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        beginningOfMonth = cal.get(GregorianCalendar.DAY_OF_WEEK);

        //Draw calendar
        for (int i = 1; i <= numberOfDays; i++) {
            int row = (i + beginningOfMonth - 2) / 7;
            int column = (i + beginningOfMonth - 2) % 7;
            calendarTableModel.setValueAt(i, row, column);
        }

        //Apply renderers
        calendarTable.setDefaultRenderer(calendarTable.getColumnClass(0), new tblCalendarRenderer());

    }

    //Constructors
    /**
     *
     * Sets the title of the calendar frame to "Calendar". <br>
     * Uses default calendar headers starting with Sunday.
     *
     */
    public MiniCalendar() {
        this.months = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        this.header = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        Initialize();
        this.setTitle(title);
    }

    /**
     * Test method for week!
     */
    public void getFirstDay() {
        System.out.println(header[0]);
    }

    private void setDefaultCalendarSize() {
        screenSize = Toolkit.getDefaultToolkit().getScreenSize().getSize();

        setSize(screenSize.width / 8, screenSize.height / 4);
    }

    //Setters 
    /**
     *
     * This is not recommended. The default size should work but it's here if
     * you need it.
     *
     * @param calendarSize
     *
     */
    public void setCalendarSize(Dimension calendarSize) {
        setSize(calendarSize.width, calendarSize.height);
    }

    /**
     *
     * Set the spacer to a custom value.
     *
     * @param newSpacer
     */
    public void setSpacer(int newSpacer) {
        spacer = newSpacer;
    }

    /**
     *
     * Sets the year range of the calendar.
     *
     * @param newYearRange
     */
    public void setYearRange(int newYearRange) {
        yearRange = newYearRange;
    }

    /**
     *
     * Sets the date object of the class.
     *
     * @param date
     */
    public void setDate(Date date) {
        chosenDate = date;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton buttonPressed = (JButton) e.getSource();
        if (buttonPressed == nextButton) {
            if (currentMonth == 11) { //Foward one year
                currentMonth = 0;
                currentYear += 1;
            } else { //Foward one month
                currentMonth += 1;
            }
            refreshCalendar(currentMonth, currentYear);
        } else if (buttonPressed == previousButton) {
            if (currentMonth == 0) { //Back one year
                currentMonth = 11;
                currentYear -= 1;
            } else { //Back one month
                currentMonth -= 1;
            }
            refreshCalendar(currentMonth, currentYear);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    // Table Renderer
    class tblCalendarRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            Component cell = super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            JComponent myJComp = (JComponent) cell;
            if (column == 0 || column == 6) { //Week-end
                setBackground(Color.LIGHT_GRAY);
            } else { //Week
                setBackground(new Color(255, 255, 255));
            }
            if (value != null) {
                if (Integer.parseInt(value.toString()) == day && currentMonth == month && currentYear == year) {
                    setBackground(new Color(220, 220, 255));
                }
            }

            if (selected) {
                cell.setBackground(Color.gray);
            }

            setBorder(null);
            setForeground(Color.black);
            return this;
        }
    }

}
